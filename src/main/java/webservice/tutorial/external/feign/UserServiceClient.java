package webservice.tutorial.external.feign;

import feign.Headers;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("user-service")
public interface UserServiceClient {

    @RequestMapping("/index")
    @Headers("Authorization: Bearer ")
    String getUsers();

    @PostMapping(value = "/register", consumes = "application/json")
    String register(String content);

    @PostMapping("/oauth/token")
    @Headers("Content-Type: application/json")
    String getToken(String content);

}