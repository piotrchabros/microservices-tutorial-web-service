package webservice.tutorial.controller;

import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import webservice.tutorial.external.feign.UserServiceClient;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/ajax")
public class AjaxController {

    @Autowired
    private UserServiceClient userServiceClient;

    @PostMapping("/user/register")
    public @ResponseBody String ajaxRegister(@RequestParam String email, @RequestParam String password){
        return userServiceClient.register("{\"username\":\""+email+"\", \"password\":\"" + password + "\", \"enabled\": \"true\"}");
    }

    @ExceptionHandler({ FeignException.class })
    void handleIllegalArgumentException(FeignException e, HttpServletResponse response) throws IOException {
        response.sendError(e.status());
    }
}
