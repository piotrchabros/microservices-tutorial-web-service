var registrationForm = $('#registrationForm');
var submitButton = $('#submitButton');
var emailInput = $('#email');
var password = $('#password');
var password2 = $('#password2');

$('#registrationPopup').modal();

submitButton.on('click', function () {

    if(!isValidEmail() || !isValidPassword())
    {
        return;
    }

    $.ajax('/ajax/user/register',
    {
        method: 'post',
        data: {
            "email": emailInput.val(),
            "password": password.val()
        },
        statusCode: {
            400: function (data) {
                if(data.responseJSON.message.substr("error.username") !== -1)
                {
                    //show username already exists error
                }
            }
        }
    }).done(function (data) {
        var response = JSON.parse(data);
    });
});

password2.on('keyup', function () {
    checkPasswordsInput();
});

password.on('keyup', function () {
    checkPasswordsInput();
});

var checkPasswordsInput = function () {

    if(!isValidPassword())
    {
        $('.invalid-feedback.password').css('display', 'block');
        $('.valid-feedback.password').css('display', 'none');
        submitButton.addClass('disabled');
    }
    else
    {
        $('.invalid-feedback.password').css('display', 'none');
        $('.valid-feedback.password').css('display', 'block');
        if(isValidEmail())
        {
            submitButton.removeClass('disabled');
        }
    }
}

emailInput.on('keyup', function () {
    checkEmailInput();
});

var checkEmailInput = function ()
{
    if(!isValidEmail())
    {
        $('.invalid-feedback.email').html('Niepoprawny adres email');
        $('.invalid-feedback.email').css('display', 'block');
        $('.valid-feedback.email').css('display', 'none');
        submitButton.addClass('disabled');
    }
    else
    {
        $('.invalid-feedback.email').css('display', 'none');
        $('.valid-feedback.email').css('display', 'block');
        if(isValidPassword())
        {
            submitButton.removeClass('disabled');
        }
    }
}

var isValidEmail = function () {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(emailInput.val());
}

var isValidPassword = function () {
    return (password.val().length > 0 && password.val() === password2.val());
};